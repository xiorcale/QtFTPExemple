#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QFileSystemModel;
class FileTransfer;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QFileSystemModel* fileSystemModel;
    FileTransfer* fileTransfer;

    void fileSystemConfiguration();
    void treeViewConfiguration();

private slots:
    void updateLocalFilePath();
    void updateProgressBar(qint64 bytesReceived, qint64 bytesTotal);

    void on_actionAuth_triggered();
    void on_actionAbout_triggered();

    void on_pushButtonUpload_clicked();
    void on_pushButtonDownload_clicked();
};

#endif // MAINWINDOW_H
