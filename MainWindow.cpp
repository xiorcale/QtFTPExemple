#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "FileTransfer.h"

#include <QtNetwork>
#include <QFileSystemModel>
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    fileSystemConfiguration();
    treeViewConfiguration();

    fileTransfer = new FileTransfer(this); // Handle the netork transfers

    connect(ui->treeViewLocal, &QTreeView::clicked, this, &MainWindow::updateLocalFilePath);
    connect(fileTransfer, &FileTransfer::updateProgress, this, &MainWindow::updateProgressBar);
}


/*********************/
/*  Private methods  */
/*********************/

void MainWindow::fileSystemConfiguration()
{
    // Set the root of the harddrive as the root of the fileSystemModel.
    fileSystemModel = new QFileSystemModel(this);
    fileSystemModel->setRootPath(QDir::currentPath());

    // Only the text files are displayed.
    fileSystemModel->setNameFilters(QStringList("*.txt"));
    fileSystemModel->setNameFilterDisables(false);

}

void MainWindow::treeViewConfiguration()
{
    ui->treeViewLocal->setModel(fileSystemModel);
    ui->treeViewLocal->setColumnHidden(2, true); // Hide the file type
}


/*****************/
/*     Slots     */
/*****************/

void MainWindow::updateLocalFilePath()
{
    QModelIndex index = ui->treeViewLocal->currentIndex(); // Get the selected file
    ui->lineEditLocalFiles->setText(fileSystemModel->filePath(index));
}

void MainWindow::updateProgressBar(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setValue((bytesTotal+1) / (bytesReceived+1) * 100);
}

void MainWindow::on_actionAuth_triggered()
{
    fileTransfer->authenticate();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About"), tr("QtNetwork API example, using ftp for transferring files  \n --------------------------------- \n Authors: Gander, Da Silva, Vaucher"));
}

void MainWindow::on_pushButtonUpload_clicked()
{
    ui->progressBar->setValue(0);
    fileTransfer->upload(ui->lineEditLocalFiles->text(), ui->lineEditRemoteFiles->text());
}

void MainWindow::on_pushButtonDownload_clicked()
{
    ui->progressBar->setValue(0);
    fileTransfer->download(ui->lineEditRemoteFiles->text(), ui->lineEditLocalFiles->text());
}

/****************/
/*  Destructor  */
/****************/

MainWindow::~MainWindow()
{
    delete ui;
}



