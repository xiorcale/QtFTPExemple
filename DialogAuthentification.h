#ifndef DIALOGAUTHENTIFICATION_H
#define DIALOGAUTHENTIFICATION_H

#include <QDialog>

namespace Ui {
class DialogAuthentification;
}

class QAuthenticator;

class DialogAuthentification : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAuthentification(QString* authUsername, QString* authPassword, QWidget *parent = 0);
    ~DialogAuthentification();

private slots:
    void on_pushButtonConnexion_clicked();

private:
    Ui::DialogAuthentification *ui;

    QString* authUsername;
    QString* authPassword;
};

#endif // DIALOGAUTHENTIFICATION_H
