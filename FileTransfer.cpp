#include "FileTransfer.h"
#include "DialogAuthentification.h"

#include <QtNetwork>
#include <QFile>
#include <QMessageBox>

FileTransfer::FileTransfer(QObject *parent) : QObject(parent)
{
    nam = new QNetworkAccessManager(this);

    authUsername = new QString();
    authPassword = new QString();

    isDownloaded = false;

    connect(nam, &QNetworkAccessManager::finished, this, &FileTransfer::onFinished);
    connect(nam, &QNetworkAccessManager::authenticationRequired, this, &FileTransfer::onAuthenticationRequired);
}

/**************************************/
/*  Public methods                    */
/**************************************/

void FileTransfer::download(QString remoteFilepath, QString localSavingPath)
{
    isDownloaded = true;

    // Construct the saving path
    this->localSavingPath = localSavingPath + remoteFilepath.mid(remoteFilepath.lastIndexOf("/"));

    // Download the file
    QNetworkReply* reply = nam->get(QNetworkRequest(QUrl(remoteFilepath)));
    connect(reply, &QNetworkReply::downloadProgress, this, &FileTransfer::updateProgress);
}

void FileTransfer::upload(QString localFilepath, QString remoteSavingPath)
{
    // Open the source file
    QFile fileSource(localFilepath);
    if (!fileSource.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::critical(0, "Error", QString("Error while opening file in readOnly"));
    }

    // Send the file
    QNetworkReply* reply = nam->put(QNetworkRequest(QUrl(remoteSavingPath + localFilepath.mid(localFilepath.lastIndexOf("/")))), fileSource.readAll());
    connect(reply, &QNetworkReply::downloadProgress, this, &FileTransfer::updateProgress);
}

void FileTransfer::authenticate()
{
    DialogAuthentification* dialogAuth = new DialogAuthentification(authUsername, authPassword);
    dialogAuth->setModal(true);
    dialogAuth->exec();
}


/**************************************/
/*  Private methods                   */
/**************************************/

void FileTransfer::onFinishedDownload(QNetworkReply* reply)
{
    // Retreiving data
    QByteArray byteArray = reply->readAll();

    // Open/create the file
    QFile file(localSavingPath);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(0, "Error", QString("Error while opening file in writeOnly"));
    }

    // Copy data inside the file, must be UTF-8 encoding
    QTextStream out(&file);
    out << QString::fromUtf8(byteArray, byteArray.size());

    file.close();
}

void FileTransfer::onFinishedUpload(QNetworkReply* reply)
{
    // Nothing to do for this exemple...
}

/**************************************/
/*  Private slots                     */
/**************************************/

void FileTransfer::onFinished(QNetworkReply* reply)
{
    // Check if it was an upload or download
    if (isDownloaded) {
        onFinishedDownload(reply);
    } else {
        onFinishedUpload(reply);
    }
    isDownloaded = false;
}

void FileTransfer::onAuthenticationRequired(QNetworkReply* reply,QAuthenticator* auth)
{
    // If the username/password are already set, there's no need to ask them again
    if (*authUsername == "" || *authPassword == "") {
        authenticate();
    }

    auth->setUser(*authUsername);
    auth->setPassword(*authPassword);
}
